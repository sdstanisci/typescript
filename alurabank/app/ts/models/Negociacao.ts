export class Negociacao{

    // atalho para nao precisar declarar os atributos da class
    constructor(readonly data: Date, readonly quantidade: number, readonly valor: number){}

    get volume(){
        return this.quantidade * this.valor;
    }
}