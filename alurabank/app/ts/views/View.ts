export abstract class View<T> {
    private _elemento: JQuery;
    private _escapar: boolean;
    
    // o ? do param escapar? significa que eh opcional
    constructor(seletor: string, escapar?: boolean) {
        
        this._elemento = $(seletor);
        this._escapar = escapar;
    }
    
    update(model: T) {
        
        let template = this.template(model);
        this._elemento.html(this.template(model));

        if(this._escapar)  template = template.replace(/<script>[\s\S]*?<\/script>/, '');

        this._elemento.html(template);
    }
    
    abstract template(model: T): string;
}